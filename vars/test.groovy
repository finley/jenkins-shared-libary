def getButtonMsg(){
    String res = ""
    def  buttons = [
            [
                    "title": "查看Jenkins流水线",
                    "actionURL": "www.baidu.com"
            ],
            [
                    "title": "代码扫描结果",
                    "actionURL": "http://sonar.luffy.com/dashboard?id="
            ]
    ]
    buttons.each() {
        if(res == ""){
            res = "   \n"
        }
        res = "${res} ---> ["+it["title"]+"]("+it["actionURL"]+") \n"
    }
    println(res)
}

//getButtonMsg()

def toBranch() {
    BRANCH_NAME = "release-"
    namespace = "dev"


    if (BRANCH_NAME =~ "release-1") {
        namespace = "release-1"
    } else
    if (BRANCH_NAME =~ "release-2") {
        namespace = "release-2"
    } else
    if (BRANCH_NAME =~ "release-3") {
        namespace = "release-3"
    }

    print(namespace)
}



toBranch()
